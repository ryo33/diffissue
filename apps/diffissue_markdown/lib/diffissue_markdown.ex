defmodule DiffissueMarkdown do
  @moduledoc """
  Documentation for DiffissueMarkdown.
  """

  @doc """
  Parse a markdown
  """
  def parse(markdown) do
    case DiffissueMarkdown.Parser.parse(markdown) do
      {:ok, result, _remainder, _position} -> {:ok, result}
      {:error, message, position} -> {:error, message, position}
    end
  end
end
