defmodule DiffissueMarkdown.Parser do
  use Parselix
  import Parselix.Basic

  def parse(markdown) do
    Parselix.parse(parse_markdown(), markdown)
  end

  parserp :parse_markdown do
    many(issue())
    |> map(fn issues ->
      Enum.map(issues, fn issue ->
        id = if issue.id == nil do
          issue.title
        else
          issue.id
        end
        {id, Diffissue.Issue.new(
          title: issue.title,
          description: issue.description,
          labels: issue.labels
        )}
      end)
      |> Enum.into(%{})
      |> Diffissue.IssueSet.new()
    end)
  end

  parserp :issue do
    [title(), default(id(), nil), default(labels(), []), description()]
    |> sequence
    |> map(fn [title, id, labels, description] ->
      %{
        title: title,
        id: id,
        labels: labels,
        description: description
      }
    end)
  end

  parserp :line do
    [not_char("#\n"), until_newline(), newline()]
    |> sequence
    |> concat
  end

  parserp :blank_line do
    newline()
  end

  parserp :description do
    [line(), blank_line()]
    |> choice
    |> many
    |> concat
    |> map(fn string ->
      String.trim_trailing(string, "\n") <> "\n"
    end)
  end

  parserp :title do
    [string("# "), until_newline(), newline()]
    |> sequence
    |> pick(1)
  end

  parserp :id do
    [string("id:"), spaces(), until_newline(), newline()]
    |> sequence
    |> pick(2)
  end

  parserp :label do
    concat(many_1(not_char(",\n")))
    |> map(&String.trim(&1, " "))
  end

  parserp :comma_separated_labels do
    [label(), many(sequence([char(","), label()]) |> pick(1))]
    |> sequence
    |> flat_once
    |> default([])
  end

  parserp :labels do
    [string("labels:"), comma_separated_labels(), newline()]
    |> sequence
    |> pick(1)
  end

  parserp :newline do
    char("\n")
  end

  parserp :until_newline do
    not_char("\n")
    |> many_1
    |> concat
  end

  parserp :spaces do
    char(" ")
    |> many
  end
end
