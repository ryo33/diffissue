defmodule DiffissueMarkdownTest do
  use ExUnit.Case
  doctest DiffissueMarkdown
  alias Diffissue.IssueSet
  alias Diffissue.Issue

  test "from markdown" do
    markdown = """
    # Problem 1
    Problem 1 description

    * item 1
    * item 2
    * item 3

    # Problem 2
    id: P2
    labels: Label 1, Label 2
    Problem 2 description
    """
    {:ok, issue_set} = DiffissueMarkdown.parse(markdown)
    assert IssueSet.get(issue_set, "Problem 1") == Issue.new(
      title: "Problem 1",
      labels: [],
      description: """
      Problem 1 description

      * item 1
      * item 2
      * item 3
      """
    )
    assert IssueSet.get(issue_set, "P2") == Issue.new(
      title: "Problem 2",
      labels: ["Label 1", "Label 2"],
      description: """
      Problem 2 description
      """
    )
  end
end
