# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

alias Diffissue.Issue

config :diffissue,
  issue_params: [
    title: Issue.StringParam,
    description: Issue.StringParam,
    labels: Issue.UnorderedListParam
  ]
