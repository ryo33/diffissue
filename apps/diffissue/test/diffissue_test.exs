defmodule DiffissueTest do
  use ExUnit.Case
  doctest Diffissue
  alias Diffissue.Issue
  alias Diffissue.IssueSet

  test "diff_issues returns Issue.Diff" do
    old = Issue.new(
      title: "old title",
      description: "a description",
      labels: ["A", "B"]
    )
    new = Issue.new(
      title: "new title",
      description: "a description",
      labels: ["A", "B"]
    )
    diff = Diffissue.diff_issues(old, new)
    assert match?(%Diffissue.Issue.Diff{}, diff)
  end

  test "diff exact same issues" do
    issue = Issue.new(
      title: "a title",
      description: "a description",
      labels: ["A", "B"]
    )
    diff = Diffissue.diff_issues(issue, issue)
    assert diff.title == :nodiff
    assert diff.description == :nodiff
    assert diff.labels == :nodiff
  end

  test "diff issues when changing" do
    old = Issue.new(
      title: "old title",
      description: "a description",
      labels: ["A", "B"]
    )
    new = Issue.new(
      title: "new title",
      description: "a description",
      labels: ["A", "B"]
    )
    diff = Diffissue.diff_issues(old, new)
    assert diff.title == {"old title", "new title"}
    assert diff.description == :nodiff
    assert diff.labels == :nodiff
  end

  test "diff issues when old issue has unknown fields" do
    old = Issue.new(
      title: :unknown
    )
    new = Issue.new(
      title: "new title"
    )
    diff = Diffissue.diff_issues(old, new)
    assert diff.title == {:unknown, "new title"}
    assert diff.description == :nodiff
    assert diff.labels == :nodiff
  end

  test "diff issues when new issue has unknown fields" do
    old = Issue.new(
      title: "old title"
    )
    new = Issue.new(
      title: :unknown
    )
    diff = Diffissue.diff_issues(old, new)
    assert diff.title == :nodiff
    assert diff.description == :nodiff
    assert diff.labels == :nodiff
  end

  test "diff issues when removing and adding labels" do
    old = Issue.new(
      labels: ["A", "B", "C"]
    )
    new = Issue.new(
      labels: ["A", "B", "D"]
    )
    diff = Diffissue.diff_issues(old, new)
    assert diff.title == :nodiff
    assert diff.description == :nodiff
    assert diff.labels == {old.labels, new.labels}
  end

  test "diff_issue_sets returns IssueSet.Diff" do
    old = IssueSet.new(%{
      one: Issue.new(title: "old title")
    })
    new = IssueSet.new(%{
      two: Issue.new(title: "new title")
    })
    diff = Diffissue.diff_issue_sets(old, new)
    assert match?(%Diffissue.IssueSet.Diff{}, diff)
  end

  test "diff exact same issue sets" do
    issue_set = IssueSet.new(%{
      one: Issue.new(title: "a title")
    })
    diff = Diffissue.diff_issue_sets(issue_set, issue_set)
    assert diff.added == %{}
    assert diff.deleted == %{}
    assert diff.changed == %{}
  end

  test "diff issue sets when changing" do
    old = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "old description"),
      two: Issue.new(title: "a title 2")
    })
    new = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "new description"),
      two: Issue.new(title: "a title 2")
    })
    diff = Diffissue.diff_issue_sets(old, new)
    assert diff.added == %{}
    assert diff.deleted == %{}
    old = IssueSet.get(old, :one)
    new = IssueSet.get(new, :one)
    issue_diff = Diffissue.diff_issues(old, new)
    assert diff.changed == %{one: issue_diff}
  end

  test "diff issue sets when adding" do
    old = IssueSet.new(%{
      one: Issue.new(title: "a title")
    })
    new = IssueSet.new(%{
      one: Issue.new(title: "a title"),
      two: Issue.new(title: "b title")
    })
    diff = Diffissue.diff_issue_sets(old, new)
    assert diff.added == %{two: IssueSet.get(new, :two)}
    assert diff.deleted == %{}
    assert diff.changed == %{}
  end

  test "diff issue sets when deleting" do
    old = IssueSet.new(%{
      one: Issue.new(title: "a title"),
      two: Issue.new(title: "a title 2")
    })
    new = IssueSet.new(%{
      two: Issue.new(title: "a title 2")
    })
    diff = Diffissue.diff_issue_sets(old, new)
    assert diff.added == %{}
    assert diff.deleted == %{one: IssueSet.get(old, :one)}
    assert diff.changed == %{}
  end

  test "diff issue sets when adding, removing, and changing" do
    old = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "old description"),
      two: Issue.new(title: "a title 2")
    })
    new = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "new description"),
      three: Issue.new(title: "a title 3")
    })
    diff = Diffissue.diff_issue_sets(old, new)
    assert diff.added == %{three: IssueSet.get(new, :three)}
    assert diff.deleted == %{two: IssueSet.get(old, :two)}
    old = IssueSet.get(old, :one)
    new = IssueSet.get(new, :one)
    issue_diff = Diffissue.diff_issues(old, new)
    assert diff.changed == %{one: issue_diff}
  end
end
