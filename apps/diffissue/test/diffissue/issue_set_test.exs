defmodule Diffissue.IssueSetTest do
  use ExUnit.Case
  doctest Diffissue.IssueSet
  alias Diffissue.IssueSet
  alias Diffissue.Issue

  test "get issue by id" do
    issue1 = Issue.new(title: "title 1")
    issue2 = Issue.new(title: "title 2")
    issue_set = IssueSet.new(%{
      one: issue1, two: issue2
    })
    assert IssueSet.get(issue_set, :one) == issue1
    assert IssueSet.get(issue_set, :three) == nil
  end

  test "get identifiers" do
    issue_set = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "a description"),
      two: Issue.new(title: "a title", description: "a description"),
      three: Issue.new(title: "a title", description: "a description")
    })
    identifiers = IssueSet.identifiers(issue_set)
    assert length(identifiers) == 3
    assert :one in identifiers
    assert :two in identifiers
    assert :three in identifiers
  end

  test "nodiff?" do
    issue_set1 = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "old description")
    })
    issue_set2 = IssueSet.new(%{
      one: Issue.new(title: "a title", description: "new description")
    })
    diff1 = Diffissue.diff_issue_sets(issue_set1, issue_set1)
    diff2 = Diffissue.diff_issue_sets(issue_set1, issue_set2)
    assert IssueSet.Diff.nodiff?(diff1)
    refute IssueSet.Diff.nodiff?(diff2)
  end
end
