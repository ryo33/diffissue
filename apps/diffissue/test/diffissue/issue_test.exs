defmodule Diffissue.IssueTest do
  use ExUnit.Case
  doctest Diffissue.IssueSet
  alias Diffissue.Issue

  test "nodiff?" do
    issue1 = Issue.new(title: "issue1 title")
    issue2 = Issue.new(title: "issue2 title")
    diff1 = Diffissue.diff_issues(issue1, issue1)
    diff2 = Diffissue.diff_issues(issue1, issue2)
    assert Issue.Diff.nodiff?(diff1)
    refute Issue.Diff.nodiff?(diff2)
  end
end
