defmodule Diffissue do
  @moduledoc """
  Diff issues
  """

  @doc """
  Diff issues
  """
  def diff_issues(old, new) do
    Diffissue.Issue.diff(old, new)
  end

  @doc """
  Diff issue sets
  """
  def diff_issue_sets(old, new) do
    Diffissue.IssueSet.diff(old, new)
  end
end
