defmodule Diffissue.Issue do
  @params Application.get_env(:diffissue, :issue_params)
  @keys @params |> Enum.map(&Kernel.elem(&1, 0))
  @enforce_keys @keys
  defstruct @keys

  defmodule Diff do
    @keys Application.get_env(:diffissue, :issue_params) |> Enum.map(&Kernel.elem(&1, 0))
    @enforce_keys @keys
    defstruct @keys
    def nodiff?(diff) do
      Map.from_struct(diff) |> Enum.all?(fn {_key, value} -> value == :nodiff end)
    end
  end

  defmodule Param do
    @callback equal?(old :: term, new :: term) :: boolean
  end

  def new(params \\ []) do
    @keys
    |> Enum.map(fn key ->
      {key, Keyword.get(params, key, :unknown)}
    end)
    |> (&Kernel.struct(__MODULE__, &1)).()
  end

  def diff(old, new) do
    old = Map.from_struct(old)
    new = Map.from_struct(new)
    @params
    |> Enum.map(fn {key, param_module} ->
      {key, diff(old[key], new[key], param_module)}
    end)
    |> (&Kernel.struct(__MODULE__.Diff, &1)).()
  end

  defp diff(old, new, param_module) do
    case {old, new} do
      {_, :unknown} -> :nodiff
      {:unknown, new} -> {old, new}
      {old, new} -> if param_module.equal?(old, new), do: :nodiff, else: {old, new}
    end
  end
end
