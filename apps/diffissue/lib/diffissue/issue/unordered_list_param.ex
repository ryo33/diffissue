defmodule Diffissue.Issue.UnorderedListParam do
  @behaviour Diffissue.Issue.Param

  @impl Diffissue.Issue.Param
  def equal?(old, new) do
    MapSet.equal?(MapSet.new(old), MapSet.new(new))
  end
end
