defmodule Diffissue.Issue.StringParam do
  @behaviour Diffissue.Issue.Param

  @impl Diffissue.Issue.Param
  def equal?(old, new) do
    old == new
  end
end
