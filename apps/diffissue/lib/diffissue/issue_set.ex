defmodule Diffissue.IssueSet do
  @keys [:issues]
  @enforce_keys @keys
  defstruct @keys

  alias Diffissue.Issue

  defmodule Diff do
    @keys [:added, :deleted, :changed]
    @enforce_keys @keys
    defstruct @keys
    def nodiff?(diff) do
      %__MODULE__{added: %{}, deleted: %{}, changed: %{}} == diff
    end
  end

  def new(issues \\ %{}) do
    %__MODULE__{
      issues: issues
    }
  end

  def get(issue_set, identifier) do
    Map.get(issue_set.issues, identifier, nil)
  end

  def identifiers(issue_set) do
    Map.keys(issue_set.issues)
  end

  def diff(old, new) do
    old_identifiers = old.issues |> Map.keys() |> MapSet.new()
    new_identifiers = new.issues |> Map.keys() |> MapSet.new()
    added_identifiers = MapSet.difference(new_identifiers, old_identifiers)
    deleted_identifiers = MapSet.difference(old_identifiers, new_identifiers)
    might_changed_identifiers = MapSet.intersection(old_identifiers, new_identifiers)
    added = added_identifiers
            |> Enum.map(fn id -> {id, get(new, id)} end)
            |> Enum.into(%{})
    deleted = deleted_identifiers
            |> Enum.map(fn id -> {id, get(old, id)} end)
            |> Enum.into(%{})
    changed = Enum.reduce(might_changed_identifiers, %{}, fn identifier, changed ->
      old_issue = get(old, identifier)
      new_issue = get(new, identifier)
      diff = Issue.diff(old_issue, new_issue)
      if Issue.Diff.nodiff?(diff) do
        changed
      else
        Map.put(changed, identifier, diff)
      end
    end)
    %__MODULE__.Diff{
      added: added,
      deleted: deleted,
      changed: changed
    }
  end
end
