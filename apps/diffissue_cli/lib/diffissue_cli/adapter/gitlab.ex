defmodule DiffissueCLI.Adapter.GitLab do
  @behaviour DiffissueCLI.Adapter
  alias DiffissueCLI.Adapter.Issue

  @keys [:token, :uri, :project]
  @enforce_keys @keys
  defstruct @keys

  @impl DiffissueCLI.Adapter
  def option_keys, do: [:token, :uri, :project]

  @impl DiffissueCLI.Adapter
  def new_client(opts) do
    host = Keyword.get(opts, :host, "gitlab.com")
    project = Keyword.fetch!(opts, :project)
    {:ok, %__MODULE__{
      token: Keyword.fetch!(opts, :token),
      uri: "https://#{host}/api/v4",
      project: URI.encode_www_form(project)
    }}
  end

  @impl DiffissueCLI.Adapter
  def fetch_all_issues(client) do
    headers = [{"PRIVATE-TOKEN", client.token}]
    issues = HTTPoison.get!("#{client.uri}/projects/#{client.project}/issues", headers).body
             |> Poison.decode!
             |> Enum.map(fn item ->
               %Issue{
                 title: item["title"],
                 description: item["description"],
                 labels: item["labels"],
                 issue_id: item["iid"],
               }
             end)
    {:ok, issues}
  end

  @impl DiffissueCLI.Adapter
  def create_issue(client, issue) do
    query = URI.encode_query(%{
      title: issue.title,
      description: issue.description,
      labels: issue.labels |> Enum.join(","),
    })
    headers = [{"PRIVATE-TOKEN", client.token}]
    HTTPoison.post! "#{client.uri}/projects/#{client.project}/issues?#{query}", "", headers
    :ok
  end

  @impl DiffissueCLI.Adapter
  def modify_issue(client, adapter_issue, diff) do
    params = %{}
    params = case diff.title do
      {_old, new} -> Map.put(params, :title, new)
      _ -> params
    end
    params = case diff.description do
      {_old, new} -> Map.put(params, :description, new)
      _ -> params
    end
    params = case diff.labels do
      {_old, new} -> Map.put(params, :labels, new |> Enum.join(","))
      _ -> params
    end
    query = URI.encode_query(params)
    headers = [{"PRIVATE-TOKEN", client.token}]
    HTTPoison.put! "#{client.uri}/projects/#{client.project}/issues/#{adapter_issue.issue_id}?#{query}", "", headers
    :ok
  end

  @impl DiffissueCLI.Adapter
  def delete_issue(client, adapter_issue) do
    headers = [{"PRIVATE-TOKEN", client.token}]
    HTTPoison.delete! "#{client.uri}/projects/#{client.project}/issues/#{adapter_issue.issue_id}", headers
    :ok
  end
end
