defmodule DiffissueCLI.Adapter do
  @callback option_keys() :: [atom]
  @callback new_client(opts :: keyword) :: {:ok, term} | {:error, String.t}
  @callback fetch_all_issues(client :: term) :: {:ok, list(%__MODULE__.Issue{})} | {:error, String.t}
  @callback create_issue(client :: term, issue :: %Diffissue.Issue{}) :: :ok | {:error, String.t}
  @callback modify_issue(client :: term, adapter_issue :: %__MODULE__.Issue{}, diff :: %Diffissue.Issue.Diff{}) :: :ok | {:error, String.t}
  @callback delete_issue(client :: term, adapter_issue :: %__MODULE__.Issue{}) :: :ok | {:error, String.t}

  defmodule Issue do
    @keys [:title, :description, :labels, :issue_id]
    @enforce_keys @keys
    defstruct @keys
  end
end
