defmodule DiffissueCLI do
  @moduledoc """
  Documentation for DiffissueCLI.
  """

  @doc """
  
  """
  def main(args \\ []) do
    adapter = Application.get_env(:diffissue_cli, :adapter)
    switches = adapter.option_keys()
               |> Enum.map(fn key -> {key, :string} end)
    args
    |> OptionParser.parse!(strict: switches)
    |> handle(adapter)
  end

  def handle({opts, [new]}, adapter) do
    with {:ok, file} <- File.read(new),
         {:ok, issue_set} <- DiffissueMarkdown.parse(file),
         {:ok, client} <- adapter.new_client(opts),
         identifiers <- Diffissue.IssueSet.identifiers(issue_set),
         additions <- Enum.map(identifiers, fn id -> Diffissue.IssueSet.get(issue_set, id) end) do
      additions
      |> Enum.map(fn issue ->
        case adapter.create_issue(client, issue) do
          :ok -> :ok
          {:error, message} -> error(message)
        end
      end)
    else
      {:error, message} -> error(message)
      {:error, message, _position} -> error(message)
    end
  end

  def handle({opts, [old, new]}, adapter) do
    with {:ok, old_file} <- File.read(old),
         {:ok, new_file} <- File.read(new),
         {:ok, old_issue_set} <- DiffissueMarkdown.parse(old_file),
         {:ok, new_issue_set} <- DiffissueMarkdown.parse(new_file),
         diff <- Diffissue.diff_issue_sets(old_issue_set, new_issue_set),
         {:ok, client} <- adapter.new_client(opts),
         {:ok, adapter_issues} <- adapter.fetch_all_issues(client),
         additions <- Enum.map(diff.added, fn {_id, issue} -> issue end),
         deletions <- Enum.map(diff.deleted, fn {_id, issue} ->
           adapter_issue = find_adapter_issue(adapter_issues, issue)
           adapter_issue
         end),
         changes <- Enum.map(diff.changed, fn {id, diff} ->
           issue = Diffissue.IssueSet.get(old_issue_set, id)
           adapter_issue = find_adapter_issue(adapter_issues, issue)
           {adapter_issue, diff}
         end) do
      additions
      |> Enum.map(fn issue ->
        case adapter.create_issue(client, issue) do
          :ok -> :ok
          {:error, message} -> error(message)
        end
      end)
      deletions
      |> Enum.map(fn adapter_issue ->
        case adapter.delete_issue(client, adapter_issue) do
          :ok -> :ok
          {:error, message} -> error(message)
        end
      end)
      changes
      |> Enum.map(fn {adapter_issue, diff} ->
        case adapter.modify_issue(client, adapter_issue, diff) do
          :ok -> :ok
          {:error, message} -> error(message)
        end
      end)
    else
      {:error, message} -> error(message)
      {:error, message, _position} -> error(message)
    end
  end

  defp find_adapter_issue(adapter_issues, issue) do
    adapter_issue = Enum.find(adapter_issues, fn
      %DiffissueCLI.Adapter.Issue{title: title} -> title == issue.title
    end)
    if is_nil(adapter_issue) do
      error("can't find a issue with the title #{issue.title}")
    end
    adapter_issue
  end

  defp error(message) do
    IO.warn(message)
    exit({:shutdown, 1})
  end
end
