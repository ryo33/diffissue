defmodule DiffissueCLITest do
  use ExUnit.Case
  doctest DiffissueCLI
  import Mox
  setup :verify_on_exit!

  test "with old and new markdown" do
    Mox.defmock(DiffissueCLI.Adapter.Test, for: DiffissueCLI.Adapter)
    Temp.track!
    {:ok, old_fd, old_file_path} = Temp.open "old"
    {:ok, new_fd, new_file_path} = Temp.open "new"
    IO.write old_fd, """
    # Issue 1
    id: 1
    labels: a, b
    This is 1.
    # Issue 2
    id: 2
    labels: a
    This is 2.
    # Issue 3
    id: 3
    labels: a
    This is 3.
    """
    IO.write new_fd, """
    # Issue 1
    id: 1
    labels: a, b
    This is 1.
    # Issue 2
    id: 2
    labels: a, b
    This is 2.
    # Issue 4
    id: 4
    labels: a
    This is 4.
    """
    test_dummy_client = %{dummy: true}
    adapter_issue1 = %DiffissueCLI.Adapter.Issue{
      title: "Issue 1",
      description: "This is 1.\n",
      labels: ["a", "b"],
      issue_id: 1
    }
    adapter_issue2 = %DiffissueCLI.Adapter.Issue{
      title: "Issue 2",
      description: "This is 2.\n",
      labels: ["a"],
      issue_id: 2
    }
    adapter_issue3 = %DiffissueCLI.Adapter.Issue{
      title: "Issue 3",
      description: "This is 3.\n",
      labels: ["a"],
      issue_id: 3
    }
    all_issues = [adapter_issue1, adapter_issue2, adapter_issue3]
    DiffissueCLI.Adapter.Test
    |> expect(:option_keys, fn -> [] end)
    |> expect(:new_client, fn [] -> {:ok, test_dummy_client} end)
    |> expect(:fetch_all_issues, fn client ->
      assert client == test_dummy_client
      {:ok, all_issues}
    end)
    |> expect(:create_issue, fn client, issue ->
      assert client == test_dummy_client
      assert issue == Diffissue.Issue.new(
        title: "Issue 4",
        description: "This is 4.\n",
        labels: ["a"]
      )
      :ok
    end)
    |> expect(:modify_issue, fn client, issue, diff ->
      assert client == test_dummy_client
      assert issue == adapter_issue2
      assert diff == Diffissue.diff_issues(
        Diffissue.Issue.new(labels: ["a"]),
        Diffissue.Issue.new(labels: ["a", "b"])
      )
      :ok
    end)
    |> expect(:delete_issue, fn client, issue ->
      assert client == test_dummy_client
      assert issue == adapter_issue3
      :ok
    end)
    DiffissueCLI.main([old_file_path, new_file_path])
  end

  test "with only new markdown" do
    Mox.defmock(DiffissueCLI.Adapter.Test, for: DiffissueCLI.Adapter)
    Temp.track!
    {:ok, new_fd, new_file_path} = Temp.open "new"
    IO.write new_fd, """
    # Issue 1
    id: 1
    labels: a
    This is 1.
    # Issue 2
    id: 2
    labels: a, b
    This is 2.
    # Issue 3
    id: 3
    labels: a, b, c
    This is 3.
    """
    test_dummy_client = %{dummy: true}
    DiffissueCLI.Adapter.Test
    |> expect(:option_keys, fn -> [] end)
    |> expect(:new_client, fn [] -> {:ok, test_dummy_client} end)
    |> expect(:create_issue, fn client, issue ->
      assert client == test_dummy_client
      assert issue == Diffissue.Issue.new(
        title: "Issue 1",
        description: "This is 1.\n",
        labels: ["a"]
      )
      :ok
    end)
    |> expect(:create_issue, fn client, issue ->
      assert client == test_dummy_client
      assert issue == Diffissue.Issue.new(
        title: "Issue 2",
        description: "This is 2.\n",
        labels: ["a", "b"]
      )
      :ok
    end)
    |> expect(:create_issue, fn client, issue ->
      assert client == test_dummy_client
      assert issue == Diffissue.Issue.new(
        title: "Issue 3",
        description: "This is 3.\n",
        labels: ["a", "b", "c"]
      )
      :ok
    end)
    DiffissueCLI.main([new_file_path])
  end
end
