# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :diffissue_cli,
  adapter: DiffissueCLI.Adapter.GitLab

if Mix.env() == :test do
  config :diffissue_cli,
    adapter: DiffissueCLI.Adapter.Test
end
