defmodule DiffissueCLI.MixProject do
  use Mix.Project

  def project do
    [
      app: :diffissue_cli,
      version: "0.1.0",
      escript: escript(),
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  defp escript do
    [main_module: DiffissueCLI]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:httpoison],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:diffissue, in_umbrella: true},
      {:diffissue_markdown, in_umbrella: true},
      {:poison, "~> 3.1"},
      {:httpoison, "~> 1.2"},
      {:mox, "~> 0.3", only: :test},
      {:temp, "~> 0.4", only: :test},
    ]
  end
end
